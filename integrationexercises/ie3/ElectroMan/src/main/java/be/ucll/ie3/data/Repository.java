package be.ucll.ie3.data;

import android.app.Application;

public class Repository {
    private final UserDao userDao;
    private final WorkOrderDao workOrderDao;

    public Repository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        userDao = db.userDao();
        workOrderDao = db.workOrderDao();
    }

    public void insertUser(User... users) {
        userDao.insertAll(users);
    }

    public User getUser(String username) {
        return userDao.getByUsername(username);
    }

    public int getTotalUsers() {
        return userDao.getCount();
    }

    public UserWithWorkOrders getWorkOrdersForUser(String username) {
        return userDao.getWorkOrders(username);
    }

    public WorkOrder getWorkorder(int... id) {
       return workOrderDao.loadAllByIds(id).get(0);
    }

    public int getTotalWorkorders() {
        return workOrderDao.getCount();
    }

    public void updateUser(User user) {
        userDao.update(user);
    }

    public void updateWorkOrder(WorkOrder workOrder) {
        workOrderDao.update(workOrder);
    }

    public void insertWorkOrder(User user, WorkOrder... workOrders) {
        for (WorkOrder workOrder : workOrders) {
            workOrder.setUserId(user.getId());
        }
        workOrderDao.insertAll(workOrders);
    }
}
