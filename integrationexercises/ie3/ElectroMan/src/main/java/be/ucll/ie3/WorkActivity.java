package be.ucll.ie3;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import be.ucll.ie3.data.Repository;
import be.ucll.ie3.data.UserWithWorkOrders;

public class WorkActivity extends AppCompatActivity {
    private static final String TAG = "WorkActivity";
    private static final int DETAIL_REQUEST_CODE = 1;
    private UserWithWorkOrders userWithWorkOrders;
    private Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work);

        setTitle(R.string.workorders);
        init();
    }

    private void init() {
        TableLayout tableLayout = findViewById(R.id.workTable);

        TableRow.LayoutParams layoutParamsHead = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TableRow.LayoutParams layoutParamsCell = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsCell.setMargins(1, 1, 1, 1);
        TableRow.LayoutParams layoutParamsBtn = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsBtn.setMargins(1, 1, 1, 1);

        repository = new Repository(getApplication());
        userWithWorkOrders = repository.getWorkOrdersForUser(getIntent().getStringExtra("username"));

        Log.i(TAG, String.format("Username passed through Intent: %s", userWithWorkOrders.user.getUsername()));
        Log.i(TAG, String.format("Total workorders for user: %d", userWithWorkOrders.workOrders.size()));

        TextView tvHello = findViewById(R.id.tvHello);
        tvHello.setText(String.format(getString(R.string.welcome), userWithWorkOrders.user.getFirstName(), userWithWorkOrders.user.getLastName()));

        for (int n = 0; n < userWithWorkOrders.workOrders.size(); n++) {
            int lambdaN = n;
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(layoutParamsHead);
            tableRow.setOnClickListener(v -> {
                Intent detailIntent = new Intent(this, DetailActivity.class);
                detailIntent.putExtra("workorder", userWithWorkOrders.workOrders.get(lambdaN).getId());
                detailIntent.putExtra("repair", "nok");
                WorkActivity.this.startActivity(detailIntent);

                Log.i(TAG, String.format("Workorder ID passed through Intent: %d", userWithWorkOrders.workOrders.get(lambdaN).getId()));
            });

            TextView tvCity = new TextView(this);
            tvCity.setGravity(Gravity.CENTER);
            tvCity.setTextColor(Color.BLACK);
            tvCity.setLayoutParams(layoutParamsCell);
            tvCity.setText(userWithWorkOrders.workOrders.get(n).getCity());
            setTextViewPaddingInDp(tvCity, 2);
            tableRow.addView(tvCity);

            TextView tvDevice = new TextView(this);
            tvDevice.setGravity(Gravity.CENTER);
            tvDevice.setTextColor(Color.BLACK);
            tvDevice.setLayoutParams(layoutParamsCell);
            tvDevice.setText(userWithWorkOrders.workOrders.get(n).getDevice());
            setTextViewPaddingInDp(tvDevice, 2);
            tableRow.addView(tvDevice);

            TextView tvCode = new TextView(this);
            tvCode.setGravity(Gravity.CENTER);
            tvCode.setTextColor(Color.BLACK);
            tvCode.setLayoutParams(layoutParamsCell);
            tvCode.setText(userWithWorkOrders.workOrders.get(n).getProblemCode());
            setTextViewPaddingInDp(tvCode, 2);
            tableRow.addView(tvCode);

            TextView tvName = new TextView(this);
            tvName.setGravity(Gravity.CENTER);
            tvName.setTextColor(Color.BLACK);
            tvName.setLayoutParams(layoutParamsCell);
            tvName.setText(userWithWorkOrders.workOrders.get(n).getCustomerName());
            setTextViewPaddingInDp(tvName, 2);
            tableRow.addView(tvName);

            Button button = new Button(this);
            button.setMinimumHeight(0);
            button.setMinimumWidth(0);
            button.setTextSize(12);
            button.setText(userWithWorkOrders.workOrders.get(n).isProcessed() ? "Yes" : "No");
            button.setLayoutParams(layoutParamsBtn);

            if (userWithWorkOrders.workOrders.get(n).isProcessed()) {
                button.setEnabled(false);
            } else {
                button.setOnClickListener(v -> {
                    Intent detailIntent = new Intent(this, DetailActivity.class);
                    detailIntent.putExtra("workorder", userWithWorkOrders.workOrders.get(lambdaN).getId());
                    detailIntent.putExtra("repair", "ok");
                    WorkActivity.this.startActivityForResult(detailIntent, DETAIL_REQUEST_CODE);
                });
            }
            tableRow.addView(button);

            tableLayout.addView(tableRow);
        }

        TableRow tableRow = new TableRow(this);
        tableRow.setBackgroundColor(Color.BLACK);
        tableRow.setMinimumHeight(getSizeInDp(5));
        tableLayout.addView(tableRow);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == DETAIL_REQUEST_CODE) {
            userWithWorkOrders = repository.getWorkOrdersForUser(getIntent().getStringExtra("username"));

            if (resultCode == Activity.RESULT_OK) {
                Log.i(TAG, String.format("Done button clicked. Workorder ID %d passed", data.getIntExtra("id", 0)));
                recreate();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "Back button clicked");
            }
        }
    }

    private void setTextViewPaddingInDp(TextView textView, int pixels) {
        setTextViewPaddingInDp(textView, pixels, pixels,pixels, pixels);
    }

    private void setTextViewPaddingInDp(TextView textView, int top, int right, int bottom, int left) {
        textView.setPadding(getSizeInDp(left), getSizeInDp(top), getSizeInDp(right), getSizeInDp(bottom));
    }

    private int getSizeInDp(int sizeInPixels) {
        float density = getResources().getDisplayMetrics().density;

        return (int) (sizeInPixels * density + 0.5f);
    }
}