package be.ucll.ie3.data;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {User.class, WorkOrder.class}, version = AppDatabase.DATABASE_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract WorkOrderDao workOrderDao();

    private static final String TAG = "AppDatabase";
    public static final int DATABASE_VERSION = 13;
    public static final String DATABASE_NAME = "electroman";
    public static volatile AppDatabase INSTANCE;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(4);

    static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            // clearAllTables() is unreliable because it doesn't tell when the job is done, so we
            // delete the db instead
            context.deleteDatabase(DATABASE_NAME);
            Log.i(TAG, "Database deleted");

            synchronized (AppDatabase.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build();
            Log.i(TAG, "Database created");
            }
        }
        return INSTANCE;
    }
}
