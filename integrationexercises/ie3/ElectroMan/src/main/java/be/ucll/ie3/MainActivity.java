package be.ucll.ie3;

import androidx.appcompat.app.AppCompatActivity;

import be.ucll.ie3.data.Repository;
import be.ucll.ie3.data.User;
import be.ucll.ie3.data.WorkOrder;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Repository repository = new Repository(getApplication());

        User user = new User();
        user.setUsername("ikke");
        user.setFirstName("Jef");
        user.setLastName("Brol");
        user.setPassword("test");

        repository.insertUser(user);
        Log.i(TAG, String.format("Total users: %d", repository.getTotalUsers()));

        // Load the user again to get the ID, otherwise foreign key constraint error occurs
        user = repository.getUser(user.getUsername());

        WorkOrder workOrder = new WorkOrder();
        workOrder.setCity("Brussels");
        workOrder.setCustomerName("Chris");
        workOrder.setDevice("Microwave");
        workOrder.setProblemCode("12");
        workOrder.setDetailedProblemDescription("Does't start");
        workOrder.setUserId(user.getId());

        WorkOrder workOrder1 = new WorkOrder();
        workOrder1.setCity("Leuven");
        workOrder1.setCustomerName("Ann");
        workOrder1.setDevice("Fridge");
        workOrder1.setProblemCode("7");
        workOrder1.setDetailedProblemDescription("Doesn't open");
        workOrder1.setUserId(user.getId());

        WorkOrder workOrder2 = new WorkOrder();
        workOrder2.setCity("Brussel");
        workOrder2.setCustomerName("Li");
        workOrder2.setDevice("Laptop");
        workOrder2.setProblemCode("5");
        workOrder2.setDetailedProblemDescription("Doesn't run Crysis properly");
        workOrder2.setUserId(user.getId());

        WorkOrder workOrder3 = new WorkOrder();
        workOrder3.setCity("Antwerpen");
        workOrder3.setCustomerName("Friedman");
        workOrder3.setDevice("Fridge");
        workOrder3.setProblemCode("7");
        workOrder3.setDetailedProblemDescription("Doesn't produce ice");
        workOrder3.setUserId(user.getId());

        WorkOrder workOrder4 = new WorkOrder();
        workOrder4.setCity("Tienen");
        workOrder4.setCustomerName("Smith");
        workOrder4.setDevice("Stove");
        workOrder4.setProblemCode("12");
        workOrder4.setDetailedProblemDescription("Produces burnt chicken");
        workOrder4.setUserId(user.getId());

        repository.insertWorkOrder(user, workOrder, workOrder1, workOrder2, workOrder3, workOrder4);
        Log.i(TAG, String.format("Total workorders: %d", repository.getTotalWorkorders()));

        // By default show login activity
        Intent loginIntent = new Intent(this, LoginActivity.class);
        MainActivity.this.startActivity(loginIntent);
    }
}