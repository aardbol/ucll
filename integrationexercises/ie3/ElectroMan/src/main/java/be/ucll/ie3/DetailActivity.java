package be.ucll.ie3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import be.ucll.ie3.data.Repository;
import be.ucll.ie3.data.WorkOrder;

public class DetailActivity extends AppCompatActivity {
    private static final String TAG = "DetailActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setTitle(R.string.workdetails);
        init();
    }

    private void init() {
        int workorderId = getIntent().getIntExtra("workorder", 0);
        String repairDetails = getIntent().getStringExtra("repair");

        Log.i(TAG, String.format("Workorder ID passed through Intent: %d", workorderId));
        Log.i(TAG, String.format("Repair info passed through Intent: %s", repairDetails));

        EditText descText = findViewById(R.id.detailEditText);
        EditText repairText = findViewById(R.id.repairEditText);
        Button backBtn = findViewById(R.id.backButton);

        if (workorderId != 0) {
            Repository repository = new Repository(getApplication());
            WorkOrder workOrder = repository.getWorkorder(workorderId);
            Button doneBtn = findViewById(R.id.doneButton);

            if (workOrder.getRepairInformation() != null) {
                repairText.setText(workOrder.getRepairInformation());
            }
            descText.setText(workOrder.getDetailedProblemDescription());

            if (repairDetails.equals("ok")) {
                repairText.setFocusable(true);
                repairText.setClickable(true);

                doneBtn.setVisibility(View.VISIBLE);
                doneBtn.setOnClickListener(v -> {
                    if (!repairText.getText().toString().equals("")) {
                        workOrder.setRepairInformation(repairText.getText().toString());
                        workOrder.setProcessed(true);
                        repository.updateWorkOrder(workOrder);

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("id", workOrder.getId());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                });
            }
            else {
                repairText.setFocusable(false);
                repairText.setClickable(false);
                doneBtn.setVisibility(View.INVISIBLE);
            }

        }

        backBtn.setOnClickListener(v -> {
            setResult(Activity.RESULT_CANCELED);
            finish();
        });
    }
}