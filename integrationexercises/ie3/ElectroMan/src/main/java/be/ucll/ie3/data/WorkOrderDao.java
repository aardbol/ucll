package be.ucll.ie3.data;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface WorkOrderDao {
    @Query("SELECT * FROM workorders")
    List<WorkOrder> getAll();

    @Query("SELECT COUNT(*) FROM workorders")
    int getCount();

    @Query("SELECT * FROM workorders WHERE id IN(:ids)")
    List<WorkOrder> loadAllByIds(int[] ids);

    @Query("SELECT * FROM workorders WHERE user_id = :uId")
    WorkOrder getByUserId(int uId);

    @Update
    void update(WorkOrder workOrder);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(WorkOrder... workOrders);

    @Delete
    void delete(WorkOrder workOrder);
}
