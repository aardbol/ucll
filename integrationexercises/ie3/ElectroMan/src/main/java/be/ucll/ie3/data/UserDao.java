package be.ucll.ie3.data;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

@Dao
public interface UserDao {
    @Query("SELECT * FROM users")
    List<User> getAll();

    @Query("SELECT COUNT(id) FROM users")
    int getCount();

    @Query("SELECT * FROM users WHERE id IN(:ids)")
    List<User> loadAllByIds(int[] ids);

    @Query("SELECT * FROM users WHERE username = :username")
    User getByUsername(String username);

    @Query("SELECT * FROM users WHERE first_name LIKE :first AND last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);

    @Query("SELECT * FROM users WHERE username = :username")
    @Transaction
    UserWithWorkOrders getWorkOrders(String username);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(User... users);

    @Update
    void update(User user);

    @Delete
    void delete(User user);
}
