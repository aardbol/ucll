package be.ucll.ie3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.SettingInjectorService;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import be.ucll.ie3.data.Repository;
import be.ucll.ie3.data.User;

public class LoginActivity extends AppCompatActivity {
    private Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle(R.string.login);

        repository = new Repository(getApplication());
    }

    public void login(View view) {
        EditText username = findViewById(R.id.editTextUsername);
        EditText password = findViewById(R.id.editTextPassword);
        TextView textViewWarning = findViewById(R.id.textViewLoginIncorrect);

        if (authenticate(username.getText().toString(), password.getText().toString())) {
            textViewWarning.setVisibility(View.INVISIBLE);
            Intent workIntent = new Intent(this, WorkActivity.class);
            workIntent.putExtra("username", username.getText().toString());
            LoginActivity.this.startActivity(workIntent);
        } else {
            textViewWarning.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Simulates authentication via the "backend". Since there's no backend in this app, the logic
     * makes use of the database rather than hard coded Strings
     */
    private boolean authenticate(String username, String password) {
        User user = repository.getUser(username);

        return user != null && password.equals(user.getPassword());
    }
}