package be.ucll.ie3.data;

import java.util.List;

import androidx.room.Embedded;
import androidx.room.Relation;

public class UserWithWorkOrders {
    @Embedded
    public User user;

    @Relation(parentColumn = "id", entityColumn = "user_id", entity = WorkOrder.class)
    public List<WorkOrder> workOrders;
}
