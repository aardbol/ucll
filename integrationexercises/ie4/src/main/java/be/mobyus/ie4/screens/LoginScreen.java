package be.mobyus.ie4.screens;

public class LoginScreen extends AbstractScreen {

	@Override
	public String[] drawScreenInternal(String... parameters) {
		System.out.println("Enter username and password (q to quit).");
		return new String[] { readFromConsole("Username"), readFromConsole("Password") };
	}

	public static void showMissingError() {
		showError("Error: username or password was not entered");
	}

	public static void showLoginError() {
		showError("Error: username or password incorrect");
	}

	private static void showError(String msg) {
		System.out.printf("\\e[3m%s\\e[0m%n", msg);
	}
}
