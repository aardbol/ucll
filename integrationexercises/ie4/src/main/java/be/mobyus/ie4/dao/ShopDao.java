package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Eshop;

import java.util.List;

public interface ShopDao {

	/**
	 Returns a list of all shops currently in database
	 */
	List<Eshop> listAllShops();

	Eshop getEshop(long id);
}
