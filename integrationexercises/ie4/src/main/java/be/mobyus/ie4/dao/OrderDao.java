package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Customer;
import be.mobyus.ie4.entities.Order;

import java.util.List;

public interface OrderDao {

	Order getOrder(long id);

	/**
	 For a given product looks up the orders which contain this product. If no matches
	 are found an empty List is returned
	 */
	List<Order> findOrdersForCustomer(Customer customer);

	/**
	 Saves the order in the database
	 */
	void saveOrder(Order order);
}
