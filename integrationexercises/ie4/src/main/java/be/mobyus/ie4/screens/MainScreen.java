package be.mobyus.ie4.screens;

public class MainScreen extends AbstractScreen {

    @Override
    public String[] drawScreenInternal(String... parameters) {
        System.out.printf("Welcome %s, you are now shopping in %s.", parameters[0], parameters[1]);
        System.out.println("Make you selection (q to quit):\n");
        System.out.println("1. Make order");
        System.out.println("2. List orders");
        return new String[] { readFromConsole("Input") };
    }
}
