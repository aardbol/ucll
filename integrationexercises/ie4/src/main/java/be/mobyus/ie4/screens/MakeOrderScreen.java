package be.mobyus.ie4.screens;

import be.mobyus.ie4.entities.Product;
import be.mobyus.ie4.service.ShopService;

import java.util.List;

public class MakeOrderScreen extends AbstractScreen {

    private final ShopService shopService;

    public MakeOrderScreen(ShopService shopService) {
        this.shopService = shopService;
    }

    @Override
    public String[] drawScreenInternal(String... parameters) {
        System.out.printf("Welcome %s, you are now shopping in %s.", parameters[0], parameters[1]);

        if (parameters.length == 3) {
            List<Product> productList = getProducts(parameters[2]);

            System.out.printf("Searching for products with name “%s”.", parameters[2]);
            System.out.println("To add products to the order now, enter the items comma separated. " +
                    "e. “1,2” will add product one and two to the order. Enter 0 to go back to the previous screen (q to quit).");

            for (int n = 0; n < productList.size(); n++) {
                System.out.printf("%d. %s", n, productList.get(n).getName());
            }
        }
        return new String[] { readFromConsole("Input") };
    }

    private List<Product> getProducts(String productName) {
        return shopService.findProducts("%" + productName + "%");
    }
}
