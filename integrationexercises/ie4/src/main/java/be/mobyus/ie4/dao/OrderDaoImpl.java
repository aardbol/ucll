package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Customer;
import be.mobyus.ie4.entities.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDaoImpl implements OrderDao{

    private final SessionFactory sessionFactory;

    public OrderDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Order getOrder(long id) {
        Query<Order> query = getSession().createQuery("from orders o where o.id = :id", Order.class);
        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Override
    public List<Order> findOrdersForCustomer(Customer customer) {
        Query<Order> query = getSession().createQuery("select c.orders from Customer c where c.id = :id", Order.class);
        query.setParameter("id", customer.getId());

        return query.list();
    }

    @Override
    public void saveOrder(Order order) {
        getSession().save(order);
    }
}
