package be.mobyus.ie4.screens;

import be.mobyus.ie4.entities.Customer;
import be.mobyus.ie4.entities.Order;
import be.mobyus.ie4.entities.OrderDetail;
import be.mobyus.ie4.service.ShopService;

import java.util.ArrayList;
import java.util.List;

public class ListOrderScreen extends AbstractScreen {

    private final ShopService shopService;

    public ListOrderScreen(ShopService shopService) {
        this.shopService = shopService;
    }

    @Override
    public String[] drawScreenInternal(String... parameters) {
        List<Order> orders = getOrders(parameters[0]);

        System.out.printf("Welcome %s, you are now shopping in %s", parameters[0], parameters[1]);
        System.out.println("You have made following orders:");
        System.out.println("Press enter to go back (q to quit)\n");

        for (int n = 0; n < orders.size(); n++) {
            System.out.printf("%d. %f.00", n, orders.get(n).getOrderTotal());
            List<OrderDetail> orderDetails = new ArrayList<>(orders.get(n).getOrderDetails());

            for (int l = 'a', i = 0; i < orderDetails.size(); l++, i++) {
                System.out.printf("%s. %s", l, orderDetails.get(i).getProduct().getName());
            }
        }
        return new String[] { readFromConsole("Input") };
    }

    private List<Order> getOrders(String username) {
        Customer customer = getCustomer(username);

        return shopService.findOrdersForCustomer(customer);
    }

    private Customer getCustomer(String username) {
        return shopService.listCustomers(null, null, username).get(0);
    }
}
