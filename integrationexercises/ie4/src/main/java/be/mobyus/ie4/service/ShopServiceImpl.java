package be.mobyus.ie4.service;

import be.mobyus.ie4.dao.CustomerDao;
import be.mobyus.ie4.dao.OrderDao;
import be.mobyus.ie4.dao.ProductDao;
import be.mobyus.ie4.dao.ShopDao;
import be.mobyus.ie4.entities.Customer;
import be.mobyus.ie4.entities.Eshop;
import be.mobyus.ie4.entities.Order;
import be.mobyus.ie4.entities.Product;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ShopServiceImpl implements ShopService {

	private ShopDao shopDao;
	private OrderDao orderDao;
	private ProductDao productDao;
	private CustomerDao customerDao;

	public ShopServiceImpl(ShopDao shopDao, OrderDao orderDao, ProductDao productDao, CustomerDao customerDao) {
		this.shopDao = shopDao;
		this.orderDao = orderDao;
		this.productDao = productDao;
		this.customerDao = customerDao;
	}

	@Override
	public List<Eshop> listEshops() {
		return shopDao.listAllShops();
	}

	@Override
	public List<Customer> listCustomers(String firstname, String lastname, String username) {
		return customerDao.findCustomers(lastname, firstname, username);
	}

	@Override
	public List<Product> findProducts(String productName) {
		return productDao.findProducts(productName);
	}

	@Override
	public List<Order> findOrdersForCustomer(Customer customer) {
		return orderDao.findOrdersForCustomer(customer);
	}

	@Override
	public Order getOrder(long id) {
		return orderDao.getOrder(id);
	}

	@Override
	public Eshop getEshop(long id) {
		return shopDao.getEshop(id);
	}

	@Override
	public Product getProduct(long id) {
		return productDao.getProduct(id);
	}

	@Override
	public Product getProduct(String name) {
		return productDao.getProduct(name);
	}

	@Override
	public void saveOrder(Order order) {
		orderDao.saveOrder(order);
	}
}