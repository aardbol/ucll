package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Customer;

import java.util.List;

public interface CustomerDao {

    /**
     Finds one or more customers that contain one of the given criteria
     At least one criteria is required, if a criteria is null or empty string it is not used in the query.
     Multiple criteria’s form an AND relation. For example, if both username and firstname
     is given, then only customers having that firsname AND userid are returned. If no matches
     are found an empty List is returned.
     */

    List<Customer> findCustomers(String name, String firstName, String username);
}
