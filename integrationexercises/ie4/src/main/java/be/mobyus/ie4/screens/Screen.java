package be.mobyus.ie4.screens;

public interface Screen {

	String[] drawScreen(String... parameters);

}
