package be.mobyus.ie4.app;

import be.mobyus.ie4.entities.*;
import be.mobyus.ie4.screens.*;
import be.mobyus.ie4.service.ShopService;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class EshopApplicationController {

	private final ShopService shopService;

	public EshopApplicationController(ShopService shopService) {
		this.shopService = shopService;
	}

	public void run() {
		Eshop selectedShop;
		String[] usernamePassword;

		while (true) {
			usernamePassword = getUsernamePassword();

			if (usernamePassword.length < 2 || usernamePassword[0].equals("") || usernamePassword[1].equals("")) {
				LoginScreen.showMissingError();
			} else {
				List<Customer> customers = shopService.listCustomers(null, null, usernamePassword[0]);

				if (customers == null || customers.size() == 0 || !customers.get(0).getPassword().equals(usernamePassword[1])) {
					LoginScreen.showLoginError();
				} else {
					// Login successful
					selectedShop = getEshop(usernamePassword[0]);
					break;
				}
			}
		}

		while (true) {
			String selection = showMainScreen(usernamePassword[0], selectedShop.getName());

			// Make an order
			if (selection.equals("1")) {
				String productName = showOrderScreen(usernamePassword[0], selectedShop.getName());

				String productSelection = showOrderScreen(
						usernamePassword[0],
						selectedShop.getName(),
						productName
				);

				if (!productSelection.equals("0")) {
					String[] products = productName.split(",");
					Order order = new Order();
					List<OrderDetail> orderDetails = new ArrayList<>();

					for (String productId : products) {
						OrderDetail orderDetail = new OrderDetail();
						Product product = shopService.getProduct(productId);
						orderDetail.setProduct(product);
						orderDetail.setAmount(BigDecimal.valueOf(1));
						orderDetail.setTotal(product.getPrice());
						order.setOrderTotal(order.getOrderTotal().add(product.getPrice()));
						orderDetails.add(orderDetail);
					}
					order.setOrderDetails(orderDetails);
					order.setEshop(selectedShop);
					order.setPaymentMethod("card");
					shopService.saveOrder(order);
				}
				// list orders
			} else if (selection.equals("2")) {
				String listOrderSelection = showListOrderScreen(usernamePassword[0], selectedShop.getName());
			}
		}
	}

	private String[] getUsernamePassword() {
		return new LoginScreen().drawScreen();
	}

	private Eshop getEshop(String... parameters) {
		String shopSelection = new ShopSelectionScreen(shopService).drawScreen(parameters)[0];

		return shopService.getEshop(Integer.parseInt(shopSelection));
	}

	private String showMainScreen(String... parameters) {
		return new MainScreen().drawScreen(parameters)[0];
	}

	private String showOrderScreen(String... parameters) {
		return new MakeOrderScreen(shopService).drawScreen(parameters)[0];
	}

	private String showListOrderScreen(String... parameters) {
		return new ListOrderScreen(shopService).drawScreen(parameters)[0];
	}
}