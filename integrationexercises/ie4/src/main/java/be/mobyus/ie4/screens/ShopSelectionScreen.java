package be.mobyus.ie4.screens;

import be.mobyus.ie4.entities.Eshop;
import be.mobyus.ie4.service.ShopService;

import java.util.List;

public class ShopSelectionScreen extends AbstractScreen {

    private final ShopService shopService;

    public ShopSelectionScreen(ShopService shopService) {
        this.shopService = shopService;
    }

    @Override
    public String[] drawScreenInternal(String... parameters) {
        String username = parameters[0];
        System.out.printf("Welcome %s, please choose your shop (q to quit):\n", username);

        List<Eshop> eshops = getEshops();

        for (int n = 1; n <= eshops.size(); n++) {
            System.out.printf("%d. %s", n, eshops.get(n).getName());
        }

        return new String[] { readFromConsole("Input") };
    }

    private List<Eshop> getEshops() {
        return shopService.listEshops();
    }
}
