package be.mobyus.ie4.entities;

import be.mobyus.util.PasswordConverter;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true, length = 50)
    private String username;
    @Convert(converter = PasswordConverter.class)
    private PasswordConverter password;
    private String name;
    private String firstName;
    @OneToMany(cascade = CascadeType.ALL)
    private Collection<Order> orders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    public Collection<Order> getOrders() {
        return orders;
    }

    public void setOrders(Collection<Order> orders) {
        this.orders = orders;
    }

    public PasswordConverter getPassword() {
        return password;
    }

    public void setPassword(PasswordConverter password) {
        this.password = password;
    }
}
