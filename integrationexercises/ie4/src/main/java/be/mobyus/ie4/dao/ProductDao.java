package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Product;

import java.util.List;

public interface ProductDao {

	/**
	 Looks up the given product by name, a wildcard match is executed automatically
	 ex. Add the “%” behind the productname and make it a “like” instead of “=” query
	 */
	List<Product> findProducts(String productName);

	Product getProduct(long id);
	Product getProduct(String name);
}
