package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Customer;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    private final SessionFactory sessionFactory;

    public CustomerDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Customer> findCustomers(String name, String firstName, String username) {
        String initialQuery = "from Customer c where ";
        List<String> wherePart = new ArrayList<>();
        Map<String, String> params = new HashMap<>();

        if (StringUtils.isNotBlank(name)) {
            wherePart.add("lower(c.name) = :name");
            params.put("name", name);
        }

        if (StringUtils.isNotBlank(firstName)) {
            wherePart.add("lower(c.firstName) = :firstName");
            params.put("firstName", firstName);
        }

        if (StringUtils.isNotBlank(username)) {
            wherePart.add("lower(c.username) = :username");
            params.put("username", username);
        }

        return getSession()
                .createQuery(initialQuery + String.join(" and ", wherePart), Customer.class)
                .setProperties(params)
                .list();
    }
}
