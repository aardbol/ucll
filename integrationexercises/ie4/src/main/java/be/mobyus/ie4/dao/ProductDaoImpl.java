package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao{

    private final SessionFactory sessionFactory;

    public ProductDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Product> findProducts(String productName) {
        Query<Product> query = getSession().createQuery("from Product p where p.name like :name", Product.class);
        query.setParameter("name", productName);

        return query.list();
    }

    @Override
    public Product getProduct(long id) {
        Query<Product> query = getSession().createQuery("from Product where id = :id", Product.class);
        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Override
    public Product getProduct(String name) {
        Query<Product> query = getSession().createQuery("from Product where name = :name", Product.class);
        query.setParameter("name", name);

        return query.getSingleResult();
    }
}
