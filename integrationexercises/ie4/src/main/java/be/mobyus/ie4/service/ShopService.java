package be.mobyus.ie4.service;

import be.mobyus.ie4.entities.Customer;
import be.mobyus.ie4.entities.Eshop;
import be.mobyus.ie4.entities.Order;
import be.mobyus.ie4.entities.Product;

import java.util.List;

public interface ShopService {

	List<Eshop> listEshops();

	List<Customer> listCustomers(String firstname, String lastname, String username);

	List<Product> findProducts(String productName);

	List<Order> findOrdersForCustomer(Customer customer);

	Order getOrder(long id);

	Eshop getEshop(long id);

	Product getProduct(long id);

	Product getProduct(String name);

	void saveOrder(Order order);
}
