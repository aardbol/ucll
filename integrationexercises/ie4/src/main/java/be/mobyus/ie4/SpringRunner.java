package be.mobyus.ie4;

import be.mobyus.ie4.app.EshopApplicationController;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringRunner {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");

		EshopApplicationController controller = (EshopApplicationController) applicationContext.getBean("eshopApplicationController");
		controller.run();

		applicationContext.close();
	}
}
