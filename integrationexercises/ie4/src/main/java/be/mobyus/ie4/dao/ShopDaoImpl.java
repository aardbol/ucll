package be.mobyus.ie4.dao;

import be.mobyus.ie4.entities.Eshop;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ShopDaoImpl implements ShopDao {

	private final SessionFactory sessionFactory;

	public ShopDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public List<Eshop> listAllShops() {
		return getSession().createQuery("from Eshop", Eshop.class).list();
	}

	@Override
	public Eshop getEshop(long id) {
		Query<Eshop> query = getSession().createQuery("from Eshop where id = :id", Eshop.class);
		query.setParameter("id", id);

		return query.getSingleResult();
	}
}
