package be.mobyus.util;

import org.springframework.security.crypto.codec.Base64;

import javax.persistence.AttributeConverter;
import java.nio.charset.StandardCharsets;

public class PasswordConverter implements AttributeConverter<String, String> {
    @Override
    public String convertToDatabaseColumn(String s) {
        return Base64.encode(s.getBytes(StandardCharsets.UTF_8)).toString();
    }

    @Override
    public String convertToEntityAttribute(String s) {
        return Base64.decode(s.getBytes(StandardCharsets.UTF_8)).toString();
    }
}
