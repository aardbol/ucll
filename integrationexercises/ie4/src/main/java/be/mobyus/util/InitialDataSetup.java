package be.mobyus.util;

import be.mobyus.ie4.entities.*;
import org.hibernate.SessionFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class InitialDataSetup {

	private final TransactionTemplate transactionTemplate;
	private final SessionFactory sessionFactory;

	public InitialDataSetup(TransactionTemplate transactionTemplate, SessionFactory sessionFactory) {
		this.transactionTemplate = transactionTemplate;
		this.sessionFactory = sessionFactory;
	}

	public void initData() {
		transactionTemplate.execute(transactionStatus -> {
			Customer customer = new Customer();
			customer.setName("snipsnop");
			customer.setFirstName("job");
			PasswordConverter password = new PasswordConverter();
			password.convertToDatabaseColumn("hothoi!58");
			customer.setPassword(password);
			customer.setUsername("js");

			Customer customer1 = new Customer();
			customer1.setName("opjontop");
			customer1.setFirstName("krek");
			PasswordConverter password1 = new PasswordConverter();
			password.convertToDatabaseColumn("8574po!58");
			customer1.setPassword(password1);
			customer1.setUsername("ko");

			Address address = new Address();
			address.setStreet("astreet");
			address.setHouseNumber("15");
			address.setPostalCode("1000");
			address.setCountry("BE");
			Address address1 = new Address();
			address1.setStreet("astreet");
			address1.setHouseNumber("15");
			address1.setPostalCode("1000");
			address1.setCountry("BE");

			Eshop eshop = new Eshop();
			eshop.setName("lolshop");
			eshop.setAddress(address);
			Eshop eshop1 = new Eshop();
			eshop1.setName("lolshop");
			eshop1.setAddress(address1);

			Product product1 = new Product();
			product1.setPrice(BigDecimal.valueOf(15));
			product1.setName("chocolat");
			product1.setNumberInStock(5);
			product1.setShortName("ch");

			Product product2 = new Product();
			product1.setPrice(BigDecimal.valueOf(515));
			product1.setName("smartphone");
			product1.setNumberInStock(25);
			product1.setShortName("sp");

			Product product3 = new Product();
			product1.setPrice(BigDecimal.valueOf(9965));
			product1.setName("bitcoin");
			product1.setNumberInStock(89);
			product1.setShortName("btc");

			Product product4 = new Product();
			product1.setPrice(BigDecimal.valueOf(99.99));
			product1.setName("concertticket");
			product1.setNumberInStock(966);
			product1.setShortName("tk");

			Product product5 = new Product();
			product1.setPrice(BigDecimal.valueOf(147));
			product1.setName("schoenen");
			product1.setNumberInStock(1);
			product1.setShortName("sc");

			Order order = new Order();
			order.setEshop(eshop);
			order.setPaymentMethod("card");

			OrderDetail orderDetail = new OrderDetail();
			orderDetail.setProduct(product1);
			orderDetail.setAmount(BigDecimal.valueOf(5));

			OrderDetail orderDetail1 = new OrderDetail();
			orderDetail1.setProduct(product2);
			orderDetail1.setAmount(BigDecimal.valueOf(5));

			Set<OrderDetail> orderDetails = new HashSet<>();
			orderDetails.add(orderDetail);
			orderDetails.add(orderDetail1);
			order.setOrderDetails(orderDetails);

			Order order1 = new Order();
			order1.setEshop(eshop);
			order1.setPaymentMethod("card");

			OrderDetail orderDetail2 = new OrderDetail();
			orderDetail2.setProduct(product3);
			orderDetail2.setAmount(BigDecimal.valueOf(2));

			OrderDetail orderDetail3 = new OrderDetail();
			orderDetail3.setProduct(product4);
			orderDetail3.setAmount(BigDecimal.valueOf(8));

			Set<OrderDetail> orderDetails1 = new HashSet<>();
			orderDetails1.add(orderDetail2);
			orderDetails1.add(orderDetail3);
			order1.setOrderDetails(orderDetails1);

			Set<Order> orders = new HashSet<>();
			orders.add(order);
			customer.setOrders(orders);

			Set<Order> orders1 = new HashSet<>();
			orders1.add(order1);
			customer1.setOrders(orders1);

			sessionFactory.getCurrentSession().save(customer);
			sessionFactory.getCurrentSession().save(customer1);
			sessionFactory.getCurrentSession().save(eshop);
			sessionFactory.getCurrentSession().save(eshop1);
			sessionFactory.getCurrentSession().save(product1);
			sessionFactory.getCurrentSession().save(product2);
			sessionFactory.getCurrentSession().save(product3);
			sessionFactory.getCurrentSession().save(product4);
			sessionFactory.getCurrentSession().save(product5);
			sessionFactory.getCurrentSession().save(order);
			sessionFactory.getCurrentSession().save(order1);

			transactionStatus.flush();
			return null;
		});
	}

	public void tearDown() {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
			}
		});
	}
}
