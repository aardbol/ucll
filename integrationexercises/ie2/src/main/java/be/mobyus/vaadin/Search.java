package be.mobyus.vaadin;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.converter.StringToFloatConverter;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.navigator.View;
import com.vaadin.ui.*;

public class Search implements View {
    private GridLayout gridLayout = new GridLayout();
    private VerticalLayout verticalLayout = new VerticalLayout();

    private Float minValue;
    private Integer number;
    private String email;

    public Search(){
        Label label = new Label("Minimum amount:");
        Label label1 = new Label("Number of products:");
        Label label2 = new Label("Product name:");
        Label label3 = new Label("Maximum amount:");
        Label label4 = new Label("Delivered:");
        Label label5 = new Label("Email address:");

        TextField minAmount = new TextField();
        TextField numProducts = new TextField();
        TextField prodName = new TextField();
        TextField maxAmount = new TextField();
        TextField email = new TextField();
        CheckBox delivered = new CheckBox();

        Button clear = new Button("Clear");
        Button submit = new Button("Submit");

        clear.addClickListener(event -> {
            minAmount.clear();
            numProducts.clear();
            prodName.clear();
            maxAmount.clear();
            email.clear();
            delivered.clear();
        });

        Binder<Search> binder = new Binder<>();
        binder.forField(minAmount).withNullRepresentation("")
                .withConverter(new StringToFloatConverter("Must be a decimal number"))
                .withValidator((integer, valueContext) -> ValidationResult.ok())
                .bind(Search::getFloat, Search::setFloat);
        binder.forField(maxAmount).withNullRepresentation("")
                .withConverter(new StringToIntegerConverter("Must be a number"))
                .withValidator((integer, valueContext) -> ValidationResult.ok())
                .bind(Search::getNumber, Search::setNumber);
        binder.forField(email).withNullRepresentation("")
                .withValidator(new EmailValidator("Must be an email"))
                .bind(Search::getEmail, Search::setEmail);

        gridLayout.setColumns(6);
        gridLayout.setRows(3);
        gridLayout.addComponent(label, 0, 0);
        gridLayout.addComponent(label1, 0, 1);
        gridLayout.addComponent(label2, 0, 2);
        gridLayout.addComponent(label3, 3, 0);
        gridLayout.addComponent(label4, 3, 1);
        gridLayout.addComponent(label5, 3, 2);
        gridLayout.addComponent(minAmount, 1, 0);
        gridLayout.addComponent(numProducts, 1, 1);
        gridLayout.addComponent(prodName, 1, 2);
        gridLayout.addComponent(maxAmount, 4, 0);
        gridLayout.addComponent(delivered, 4, 1);
        gridLayout.addComponent(email, 4, 2);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(submit, clear);
        verticalLayout.addComponents(gridLayout, horizontalLayout);
    }

    public Float getFloat() {
        return minValue;
    }

    public void setFloat(Float v) {
        minValue = v;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Component getViewComponent(){
        return verticalLayout;
    }


}