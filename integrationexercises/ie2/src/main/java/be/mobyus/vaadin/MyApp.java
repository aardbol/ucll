package be.mobyus.vaadin;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

public class MyApp extends UI {
    private Login login = new Login();
    private Header header = new Header();
    private Footer footer = new Footer();
    private Search search = new Search();
    private Navigator navigator;

    @Override
    protected void init(VaadinRequest request) {
        navigator = new Navigator(this,this);
        GridLayout gridLayout = new GridLayout(1,3);
        gridLayout.setSizeFull();
        gridLayout.setRowExpandRatio(1, 1F);
        Panel panel = new Panel();
        panel.setSizeFull();

        this.navigator = new Navigator(this, panel);
        navigator.addView("login", Login.class);
        navigator.addView("search", Search.class);
        navigator.navigateTo("login");

        gridLayout.addComponents(
                header.getViewComponent(),
                panel,
                footer.getViewComponent());
        setNavigator(navigator);
        setContent(gridLayout);
    }
}
