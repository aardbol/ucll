package be.mobyus.vaadin;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class Header implements View {
    HorizontalLayout headerLayout = new HorizontalLayout();

    public Header(){
        Label label = new Label(VaadinIcons.ENVELOPE.getHtml() + "  Some cool shop to buy stuff", ContentMode.HTML);
        headerLayout.addComponent(label);
    }

    @Override
    public Component getViewComponent(){
        return headerLayout;
    }


}

