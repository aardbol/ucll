package be.mobyus.vaadin;

import com.vaadin.data.Binder;
import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;

public class Login implements View {
    private final Button button = new Button("Login");
    private Binder<TextField> binder = new Binder<>(TextField.class);

    final String name = "test";
    final String pass = "test";

    FormLayout layout = new FormLayout();
    TextField username = new TextField("Username");
    TextField password = new TextField("Password");

    public Login(){
        Label loginLabel = new Label("You are logged in");
        Label errorLabel = new Label("Invalid username and/or password");

        username.setValue("");
        username.addBlurListener(blurEvent -> {
            String tfValue = username.getValue();
            try {
                if (tfValue != null) {
                    button.setEnabled(true);
                }
            } catch (Exception e) {
                Notification notification = new Notification("Insert username", Notification.TYPE_TRAY_NOTIFICATION);
                notification.setPosition(Position.TOP_RIGHT);
                notification.show(Page.getCurrent());
                button.setEnabled(false);
            }
        });

        password.setValue("");
        password.addBlurListener(blurEvent -> {
            String tfValue1 = password.getValue();
            try {
                if (tfValue1 != null) {
                    button.setEnabled(true);
                    UI.getCurrent().getNavigator().navigateTo("search");
                }
            } catch (Exception e) {
                Notification notification = new Notification("Insert password", Notification.TYPE_TRAY_NOTIFICATION);
                notification.setPosition(Position.TOP_RIGHT);
                notification.show(Page.getCurrent());
                button.setEnabled(false);
            }
        });

        button.addClickListener(clickEvent -> {
            if (username.getValue().equals(name) && password.getValue().equals(pass)){
                layout.removeComponent(errorLabel);
                layout.addComponent(loginLabel);
            }
            else{
                layout.addComponent(errorLabel);
            }
        });


    }
    @Override
    public Component getViewComponent(){
        layout.addComponents(username, password, button);

        return layout;
    }


}
