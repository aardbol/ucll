package be.mobyus.vaadin;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class Footer implements View {
    HorizontalLayout footerLayout = new HorizontalLayout();

    public Footer(){
        Label label = new Label( VaadinIcons.COPYRIGHT.getHtml() + "  Copyright 2021 SomeCoolShop", ContentMode.HTML);
        footerLayout.addComponent(label);
    }

    @Override
    public Component getViewComponent(){
        return footerLayout;
    }


}

