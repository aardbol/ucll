package be.ucll.exercises;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultCallerLauncher;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import be.ucll.myapp.R;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Ex2_3 extends AppCompatActivity {

    private TextView textView;
    private EditText phone;
    private EditText email;
    private EditText firstname;
    private EditText lastname;

    private ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    new ActivityResultCallback<ActivityResult>() {
                        @Override
                        public void onActivityResult(ActivityResult result) {
                            if (result.getResultCode() == RESULT_OK) {
                                textView.setVisibility(View.VISIBLE);
                                textView.setTextColor(getColor(R.color.green));
                                textView.setText("Contact added!");
                            } else {
                                textView.setVisibility(View.VISIBLE);
                                textView.setTextColor(getColor(R.color.red));
                                textView.setText("Contact aborted!");
                            }
                        }
                    }
            );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex2_3);

        textView = findViewById(R.id.textView6);
        phone = (EditText) findViewById(R.id.editTextTextPersonName14);
        email = (EditText) findViewById(R.id.editTextTextPersonName13);
        firstname = (EditText) findViewById(R.id.editTextTextPersonName11);
        lastname = (EditText) findViewById(R.id.editTextTextPersonName12);
    }

    public void addContact(View view) {
        textView.setVisibility(View.INVISIBLE);
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, email.getText())
                .putExtra(ContactsContract.Intents.Insert.PHONE, phone.getText())
                .putExtra(ContactsContract.Intents.Insert.NAME, firstname.getText() + " " + lastname.getText());
        intent.putExtra("finishActivityOnSaveCompleted", true);
        activityResultLauncher.launch(intent);
    }
}