package be.ucll.exercises;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import be.ucll.myapp.R;

public class Ex4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex4);

        TextView textView = findViewById(R.id.textView);
        textView.setText(Html.fromHtml(getString(R.string.ex3_hyperlink)));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}