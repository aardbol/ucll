package be.ucll.exercises;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import be.ucll.myapp.R;

public class Ex2_2b extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex2_2b);
    }

    public void submitForm(View view) {
        EditText fn = findViewById(R.id.editTextTextPersonName7);
        EditText ln = findViewById(R.id.editTextTextPersonName9);
        EditText bd = findViewById(R.id.editTextTextPersonName10);

        Intent i = new Intent();
        i.putExtra("firstname", fn.getText().toString());
        i.putExtra("lastname", ln.getText().toString());
        i.putExtra("birthdate", bd.getText().toString());

        setResult(RESULT_OK, i);
        finish();
    }
}