package be.ucll.exercises;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import be.ucll.myapp.R;

public class Ex2_2a extends AppCompatActivity {

    LocalDate dateNow;
    ImageView imageView;
    ActivityResultLauncher<Intent> activityResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    TextView textView = findViewById(R.id.textView5);
                    Intent intent = result.getData();

                    if (result.getResultCode() == RESULT_OK && intent != null) {
                        String givenBirthdate = intent.getExtras().getString("birthdate");
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d[d]/M[M]/yyyy");
                        LocalDate birthdate = LocalDate.parse(givenBirthdate, dateTimeFormatter)
                                .withYear(dateNow.getYear());
                        birthdate = dateNow.isAfter(birthdate) ? birthdate.plusYears(1) : birthdate;
                        long monthsBetween = ChronoUnit.MONTHS.between(dateNow, birthdate);
                        long daysBetween = ChronoUnit.DAYS.between(dateNow.plusMonths(monthsBetween), birthdate);
                        String birthdayWhen = "";

                        if (monthsBetween == 0 && daysBetween == 0) {
                            birthdayWhen = getString(R.string.birthdaynow);
                            imageView.setVisibility(View.VISIBLE);
                        } else {
                            birthdayWhen = getString(R.string.birthdaynext)
                                    .replace("%number_of_months%", Long.toString(monthsBetween))
                                    .replace("%number_of_days%", Long.toString(daysBetween));
                            imageView.setVisibility(View.INVISIBLE);
                        }

                        String msg = getString(R.string.hello1);
                        msg = msg
                                .replace("%firstname%", intent.getExtras().getString("firstname"))
                                .replace("%lastname%", intent.getExtras().getString("lastname"))
                                .replace("%birthdate%", givenBirthdate)
                                .replace("%date_today%", DateTimeFormatter.ofPattern("dd-MM-yyyy").format(dateNow));
                        msg = msg + " " + birthdayWhen;

                        textView.setText(msg);
                    }
                }
            }
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex2_2a);

        dateNow = LocalDate.now();
        imageView = findViewById(R.id.imageView2);
    }

    public void openForm(View view) {
        activityResult.launch(new Intent(this, Ex2_2b.class));
        imageView.setVisibility(View.INVISIBLE);
    }
}