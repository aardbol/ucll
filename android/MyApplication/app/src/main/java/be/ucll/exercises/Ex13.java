package be.ucll.exercises;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import be.ucll.myapp.R;

public class Ex13 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex13);

        Button button = findViewById(R.id.button);
        EditText editText = findViewById(R.id.editTextTextPersonName3);
        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Log.d("ex13", editText.getText().toString());
            }
        });
    }
}