package be.ucll.introduction.ex;

import java.util.HashSet;
import java.util.Set;

public class TheQuoteCollector {

	private Quote quote;

	private Set<Quote> s = new HashSet<>();

	public void addQuote(Quote quote) {
		s.add(quote);
	}

	public boolean containsQuote(Quote quote) {
		return s.contains(quote);
	}

	public static class Quote {
		private final String quote;
		private final String owner;

		public Quote(final String quote, final String owner) {
			this.quote = quote;
			this.owner = owner;
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Quote)) {
				return false;
			}
			Quote n = (Quote) o;
			return n.quote.equals(quote) && n.owner.equals(owner);
		}
	}
}

