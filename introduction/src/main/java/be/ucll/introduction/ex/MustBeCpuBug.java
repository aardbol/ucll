package be.ucll.introduction.ex;

public class MustBeCpuBug {

	public void validateLessThanOne(float f) {
		if (f >= 1.0f) {
			throw new IllegalArgumentException("Value can't be greater than 1!");
		}
	}
}
