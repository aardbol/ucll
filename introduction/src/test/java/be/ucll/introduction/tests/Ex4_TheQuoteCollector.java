package be.ucll.introduction.tests;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import be.ucll.introduction.ex.TheQuoteCollector;
import be.ucll.introduction.ex.TheQuoteCollector.Quote;

@Test
public class Ex4_TheQuoteCollector {

	public void test() {
		TheQuoteCollector theQuoteCollector = new TheQuoteCollector();
		theQuoteCollector.addQuote(new Quote("Do. Or do not. There is no try.", "Yoda"));
		theQuoteCollector.addQuote(new Quote("Your focus determines your reality.", "Qui-Gon Jinn"));

		assertTrue(theQuoteCollector.containsQuote(new Quote("Your focus determines your reality.", "Qui-Gon Jinn")));
	}
}
