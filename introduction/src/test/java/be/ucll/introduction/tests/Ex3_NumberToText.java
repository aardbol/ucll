package be.ucll.introduction.tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import be.ucll.introduction.ex.NumberToText;

@Test
public class Ex3_NumberToText {

	private NumberToText numberToText = new NumberToText();

	public void testNumberToText() {
		assertEquals(numberToText.numberToText(155), "one hundred fifty five");
		assertEquals(numberToText.numberToText(1), "one");
		assertEquals(numberToText.numberToText(0), "zero");
		assertEquals(numberToText.numberToText(99), "ninety nine");
		assertEquals(numberToText.numberToText(100), "one hundred");
		assertEquals(numberToText.numberToText(101), "one hundred one");
		assertEquals(numberToText.numberToText(534635867), "five hundred thirty four million six hundred thirty five thousand eight hundred sixty seven");
	}
}
