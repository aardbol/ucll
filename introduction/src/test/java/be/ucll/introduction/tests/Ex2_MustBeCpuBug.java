package be.ucll.introduction.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import be.ucll.introduction.ex.MustBeCpuBug;

@Test
public class Ex2_MustBeCpuBug {

	private MustBeCpuBug mustBeCpuBug = new MustBeCpuBug();

	public void testValidateLessThanOne_1() {
		try {
			mustBeCpuBug.validateLessThanOne(0.9999f);
		} catch (IllegalArgumentException illegalArgumentException) {
			Assert.fail("Got unexpected IllegalArgumentException: 0.99 should be smaller than 1");
		}
	}

	public void getTestValidateLessThanOne_2() {
		try {
			mustBeCpuBug.validateLessThanOne(0.999999999f);
		} catch (IllegalArgumentException illegalArgumentException) {
			Assert.fail("Got unexpected IllegalArgumentException: 0.999999999 should be smaller than 1");
		}
	}
}
