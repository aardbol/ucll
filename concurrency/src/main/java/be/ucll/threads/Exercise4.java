package be.ucll.threads;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class Exercise4 {

	public static String[] words = new String[] { "this", "that", "some", "succ", "poss", "frei", "ligh", "heav",
			"over", "unde", "upsi", "neve", "besi", "luck", "neok", "neaj", "nonj", "eood", "rand", "hous", "gard",
			"tele", "comp" };

	public static void main(String[] args) throws Exception {

		long start = System.currentTimeMillis();

		// Change this number to test with more or less threads
		int numberOfThreads = 15;

		//We create a ExecutorService with a custom ThreadFactory just so we can give the created threads a name
		ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads, new ThreadFactory() {
			private final ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();
			private int threadNumber = 0;

			@Override
			public Thread newThread(Runnable r) {
				Thread thread = defaultThreadFactory.newThread(r);
				thread.setName("groept-thread-pool-" + threadNumber++);
				System.err.println("Creating thread:" + thread.getName());
				return thread;
			}
		});

		System.err.println("Firing up " + numberOfThreads + " threads, for " + words.length + " words");
		executeSearchAndDisplayResults(executorService);
		long duration = System.currentTimeMillis() - start;
		System.err.println("All done. Took:" + duration / 1000 + " seconds and " + duration % 1000 + " milliseconds");

	}

	public static void executeSearchAndDisplayResults(ExecutorService executorService) {


		//TODO IMPLEMENT ME - see Ex
	
		executorService.shutdown();
	}

	static class FileScanner implements Callable<Result> {
		private String word;

		FileScanner(String word) {
			this.word = word;
		}

		@Override
		public Result call() throws Exception {
			Result result = new Result();
			result.setWord(word);
			//TODO FIX PATH FOR YOUR MACHINE
			BufferedReader reader = new BufferedReader(new FileReader("ENTER_FILE_PATH_HERE"));
			String line = null;
			Integer matches = 0;

			while ((line = reader.readLine()) != null) {
				if (line.equalsIgnoreCase(word)) {
					matches++;
				}
			}
			result.setMatches(matches);
			reader.close();
			return result;
		};
	}

	static class Result {
		private String word;
		private int matches;

		public String getWord() {
			return word;
		}

		public void setWord(String word) {
			this.word = word;
		}

		public int getMatches() {
			return matches;
		}

		public void setMatches(int matches) {
			this.matches = matches;
		}

	}
}
