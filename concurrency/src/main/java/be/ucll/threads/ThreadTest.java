package be.ucll.threads;

public class ThreadTest {

	public static void main(String[] args) {
		System.err.println("I'm starting extra threads, I'm executing in:"+
				Thread.currentThread().getName());
		Runnable runnable = () -> {
			while (true) {
				try {
					System.err.println("I'm the logic executing in:" + Thread.currentThread().getName());
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		Thread testThread = new Thread(runnable, "groept-test-thread-1");
		testThread.start();

		while (true) {
			try {
				System.err.println("I'm the logic executing in:" + Thread.currentThread().getName());
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}


}
