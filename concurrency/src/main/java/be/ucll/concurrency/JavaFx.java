package be.ucll.concurrency;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class JavaFx extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		FXMLLoader loader = new FXMLLoader();
		Parent root = (Parent) loader.load(this.getClass().getResourceAsStream("test.fxml"));
		Scene scene = new Scene(root, 768, 480);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Concurrency exercise 3");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
