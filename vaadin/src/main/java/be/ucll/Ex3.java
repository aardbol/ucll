package be.ucll;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

public class Ex3 implements View {
    private Integer value;

    @Override
    public Component getViewComponent() {
        Label label = new Label();
        Button button = new Button("Click me");
        TextField textField = new TextField();
        Binder<Ex3> binder = new Binder<>();

        binder.forField(textField).withNullRepresentation("")
                .withConverter(new StringToIntegerConverter("Must be a number"))
                .withValidator(((integer, valueContext) -> ValidationResult.ok()))
                .bind(Ex3::getValue, Ex3::setValue);
        binder.setBean(this);

        button.addClickListener(clickEvent -> {
            if (binder.isValid()) {
                label.setValue("" + getValue());
            } else {
                label.setValue("");
            }
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.addComponents(button, textField, label);
        return horizontalLayout;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
