package be.ucll;

import com.vaadin.navigator.View;
import com.vaadin.ui.*;

public class Ex5 implements View {
    private int counter = 0;

    @Override
    public Component getViewComponent() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.setSpacing(true);
        gridLayout.setColumns(2);

        Label label = new Label();
        Label label1 = new Label();
        Label label2 = new Label();
        Button button = new Button("Click me", clickEvent -> {
            incrementCounter();
            label.setValue("" + getCounter());
        });
        Button button1 = new Button("Click me", clickEvent -> {
            incrementCounter();
            label1.setValue("" + getCounter());
        });
        Button button2 = new Button("Click me", clickEvent -> {
            incrementCounter();
            label2.setValue("" + getCounter());
        });
        Button button3 = new Button("Update all", clickEvent -> {
            incrementCounter();
            label.setValue("" + getCounter());
            label1.setValue("" + getCounter());
            label2.setValue("" + getCounter());
        });

        gridLayout.addComponents(button, label);
        gridLayout.addComponents(button1, label1);
        gridLayout.addComponents(button2, label2);
        gridLayout.addComponents(new Label(""), new Label(""));
        gridLayout.addComponents(button3);
        return gridLayout;
    }

    public int getCounter() {
        return counter;
    }

    public void incrementCounter() {
        this.counter++;
    }
}
