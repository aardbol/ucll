package be.ucll;

public class CounterModel {
    private int counter;

    public int getCounter() {
        return counter;
    }

    public String getCounterStr() {
        return Integer.toString(counter);
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
