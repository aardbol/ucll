package be.ucll;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;

public class Ex8Page1 implements View {
    private final CounterModel counterModel;

    public Ex8Page1(CounterModel counterModel) {
        this.counterModel = counterModel;
    }

    @Override
    public Component getViewComponent() {
        VerticalLayout verticalLayout = new VerticalLayout();
        Label label = new Label(counterModel.getCounterStr());
        Label label1 = new Label("page1");
        Button button1 = new Button("Next");

        button1.addClickListener((Button.ClickListener) event -> UI.getCurrent().getNavigator().navigateTo("ex2"));

        verticalLayout.addComponents(label, button1, label1);

        return verticalLayout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        counterModel.setCounter(0);
    }
}
