package be.ucll;

import com.vaadin.data.Binder;
import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.v7.data.validator.StringLengthValidator;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Label;
import com.vaadin.v7.ui.TextField;
import com.vaadin.v7.ui.VerticalLayout;

public class Ex7 implements View {

    @Override
    public Component getViewComponent()
    {
        VerticalLayout gridLayout = new VerticalLayout();
        gridLayout.setSpacing(true);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        HorizontalLayout horizontalLayout1 = new HorizontalLayout();

        TextField textField = new TextField();
        TextField textField2 = new TextField();
        TextField textField3 = new TextField();
        textField.setNullRepresentation("");
        textField2.setNullRepresentation("");
        textField3.setNullRepresentation("");
        textField.addValidator(new StringLengthValidator(
                "Max één teken toegelaten", 0, 1 , true));

        Button button = new Button("Submit");
        Button reset = new Button("Reset");
        Label label = new Label();

        button.addClickListener(event -> {
            if (textField.isValid())
            {
                label.setValue(String.format("You have entered %s %s %s",
                        textField.getValue(), textField2.getValue(), textField3.getValue()));
            }
            else {
                label.setValue("not gud");
            }
        });

        reset.addClickListener(event -> {
            label.setValue("");
            textField.setValue("");
            textField2.setValue("");
            textField3.setValue("");

        });

        horizontalLayout1.addComponents(button, reset, label);
        horizontalLayout.addComponents(textField, textField2, textField3);
        gridLayout.addComponents(horizontalLayout, horizontalLayout1);
        return gridLayout;
    }
}
