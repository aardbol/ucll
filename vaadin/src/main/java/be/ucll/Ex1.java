package be.ucll;

import com.vaadin.navigator.View;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.v7.ui.HorizontalLayout;

public class Ex1 implements View {

    @Override
    public Component getViewComponent() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        Button button = new Button("Click me");
        Label label = new Label("Hello world!");
        label.setVisible(false);

        button.addClickListener((Button.ClickListener)  e -> label.setVisible(!label.isVisible()));

        horizontalLayout.addComponents(button, label);
        return horizontalLayout;
    }
}
