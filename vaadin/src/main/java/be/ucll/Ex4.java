package be.ucll;

import com.vaadin.navigator.View;
import com.vaadin.ui.*;

public class Ex4 implements View {

    @Override
    public Component getViewComponent() {
        GridLayout gridLayout = new GridLayout(6, 8);
        gridLayout.setColumnExpandRatio(5, 1f);
        gridLayout.setRowExpandRatio(6, 1f);
        gridLayout.setSpacing(true);
        gridLayout.setMargin(true);
        gridLayout.setSizeFull();

        Panel panel1 = new Panel("Identificatiegegevens");
        Panel panel2 = new Panel("Contactgegevens");
        Label labelVoornaam = new Label("Voornaam");
        labelVoornaam.setSizeUndefined();
        Label labelAchternaam = new Label("Achternaam");
        labelAchternaam.setSizeUndefined();
        Label labelLand = new Label("Land");
        labelLand.setSizeUndefined();
        TextField textFieldVn = new TextField();
        textFieldVn.setPlaceholder("voornaam");
        TextField textFieldAn = new TextField();
        textFieldAn.setPlaceholder("achternaam");
        TextField textFieldLand = new TextField();
        textFieldLand.setPlaceholder("land");

        gridLayout.addComponent(panel1, 0,0, 1,0);
        gridLayout.addComponent(panel2, 3,0, 4,0);
        gridLayout.addComponent(labelVoornaam, 0, 1);
        gridLayout.addComponent(labelAchternaam, 0, 2);
        gridLayout.addComponent(textFieldVn, 1, 1);
        gridLayout.addComponent(textFieldAn, 1, 2);
        gridLayout.addComponent(labelLand, 3, 1);
        gridLayout.addComponent(textFieldLand, 4, 1);

        return gridLayout;
    }
}
