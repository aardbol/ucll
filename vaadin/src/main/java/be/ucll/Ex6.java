package be.ucll;

import com.vaadin.navigator.View;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LoggingPermission;

public class Ex6 implements View {
    private final List<String> options = new ArrayList<>();

    @Override
    public Component getViewComponent() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.setColumns(2);

        options.add("Antwerpen");
        options.add("Aalst");
        options.add("Leuven");

        Label label = new Label();
        ComboBox comboBox = new ComboBox("Select your city", options);
        comboBox.addValueChangeListener(valueChangeEvent -> {
            label.setValue(valueChangeEvent.getValue().toString());
        });

        gridLayout.addComponents(comboBox, label);
        return gridLayout;
    }
}
