package be.ucll;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;

public class Ex8Page2 implements View {
    private final CounterModel counterModel;

    public Ex8Page2(CounterModel counterModel) {
        this.counterModel = counterModel;
    }

    @Override
    public Component getViewComponent() {
        VerticalLayout verticalLayout = new VerticalLayout();
        Label label = new Label(counterModel.getCounterStr());
        Label label1 = new Label("page2");
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button button = new Button("Previous");
        Button button1 = new Button("Next");

        button.addClickListener((Button.ClickListener) event -> UI.getCurrent().getNavigator().navigateTo("ex1"));
        button1.addClickListener((Button.ClickListener) event -> UI.getCurrent().getNavigator().navigateTo("ex3"));

        horizontalLayout.addComponents(button, button1);
        verticalLayout.addComponents(label, horizontalLayout, label1);

        return verticalLayout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        counterModel.setCounter(1);
    }
}
