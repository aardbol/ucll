package be.ucll;

import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.v7.data.Validator;
import com.vaadin.v7.data.util.converter.Converter;
import com.vaadin.v7.data.util.converter.StringToIntegerConverter;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Label;
import com.vaadin.v7.ui.TextField;

public class Ex2 implements View {

    @Override
    public Component getViewComponent() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        Label label = new Label();
        Button button = new Button("Click me");
        TextField textField = new TextField();
        textField.setNullRepresentation("");
        textField.setConverter(Integer.class);
        textField.addBlurListener(blurEvent -> {
            String tfValue = textField.getValue();

            try {
                Integer convertedValue = (Integer) textField.getConvertedValue();
                if (convertedValue != null) {
                    button.setEnabled(true);
                }
            } catch (Converter.ConversionException e) {
                Notification notification = new Notification("Invalid integer value: " + tfValue,
                        Notification.TYPE_TRAY_NOTIFICATION);
                notification.setPosition(Position.TOP_RIGHT);
                notification.show(Page.getCurrent());
                button.setEnabled(false);
            }
        });

        button.addClickListener(clickEvent -> {
            Integer convertedValue = (Integer) textField.getConvertedValue();
            if (convertedValue != null) {
                label.setValue(convertedValue.toString());
            }
        });

        horizontalLayout.addComponents(button, textField, label);
        return horizontalLayout;
    }
}
