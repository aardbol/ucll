package be.ucll;

import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;

@Widgetset("com.vaadin.v7.Vaadin7WidgetSet")
public class MyUI extends UI {
    private final MenuBar menuBar = new MenuBar();
    private Navigator navigator;

    {
        menuBar.setWidth(100.0f, Unit.PERCENTAGE);
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        GridLayout gridLayout = new GridLayout(1, 2);
        gridLayout.setSizeFull();
        gridLayout.setRowExpandRatio(1, 1F);
        Panel panel = new Panel();
        panel.setSizeFull();

        this.navigator = new Navigator(this, panel);
        addNavigatorViews();

        navigator.navigateTo("ex7");

        gridLayout.addComponents(menuBar, panel);
        setContent(gridLayout);

//        Button button = new Button("Test");
//        button.setStyleName("redbutton");
//        setContent(button);

//        Panel panel = new Panel();
//        Navigator navigator = new Navigator(this, panel);
//
//        CounterModel counterModel = new CounterModel();
//        Ex8Page1 ex8Page1 = new Ex8Page1(counterModel);
//        Ex8Page2 ex8Page2 = new Ex8Page2(counterModel);
//        navigator.addView("ex1", ex8Page1);
//        navigator.addView("ex2", ex8Page2);
//        navigator.navigateTo("ex1");
//
//        setNavigator(navigator);
//        setContent(panel);
    }

    public void addNavigatorViews() {
        navigator.addView("ex1", Ex1.class);
        menuBar.addItem("Ex1", (selectedItem) -> navigator.navigateTo("ex1"));
        navigator.addView("ex2", Ex2.class);
        menuBar.addItem("Ex2", (selectedItem) -> navigator.navigateTo("ex2"));
        navigator.addView("ex3", Ex3.class);
        menuBar.addItem("Ex3", (selectedItem) -> navigator.navigateTo("ex3"));
        navigator.addView("ex4", Ex4.class);
        menuBar.addItem("Ex4", (selectedItem) -> navigator.navigateTo("ex4"));
        navigator.addView("ex5", Ex5.class);
        menuBar.addItem("Ex5", (selectedItem) -> navigator.navigateTo("ex5"));
        navigator.addView("ex6", Ex6.class);
        menuBar.addItem("Ex6", (selectedItem) -> navigator.navigateTo("ex6"));
        navigator.addView("ex7", Ex7.class);
        menuBar.addItem("Ex7", (selectedItem) -> navigator.navigateTo("ex7"));
    }
}
